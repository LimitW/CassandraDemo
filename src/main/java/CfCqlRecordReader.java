import com.datastax.driver.core.Row;
import org.apache.hadoop.mapreduce.RecordReader;
import java.io.IOException;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;


public class CfCqlRecordReader extends RecordReader<Long, Row>
    implements org.apache.hadoop.mapred.RecordReader<Long, Row> {
    public CfCqlRecordReader(String[] columnFamilies) {
        super();
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        return false;
    }

    @Override
    public Long getCurrentKey() throws IOException, InterruptedException {
        return null;
    }

    @Override
    public Row getCurrentValue() throws IOException, InterruptedException {
        return null;
    }

    @Override
    public boolean next(Long key, Row value) throws IOException {
        return false;
    }

    @Override
    public Long createKey() {
        return null;
    }

    @Override
    public Row createValue() {
        return null;
    }

    @Override
    public long getPos() throws IOException {
        return 0;
    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public float getProgress() throws IOException {
        return 0;
    }

    public void initialize(InputSplit s, TaskAttemptContext tac) {

    }
}
